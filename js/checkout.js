function finalReview()
{
    document.getElementById("fr_Ship_First_Name").value=document.getElementById("Ship_First_Name").value;
    document.getElementById("fr_Ship_Last_Name").value=document.getElementById("Ship_Last_Name").value;
    document.getElementById("fr_Ship_Address1").value=document.getElementById("Ship_Address1").value;
    document.getElementById("fr_Ship_Address2").value=document.getElementById("Ship_Address2").value;
    document.getElementById("fr_Ship_City").value=document.getElementById("Ship_City").value;
    document.getElementById("fr_Ship_State").value=document.getElementById("Ship_State").value;
    document.getElementById("fr_Ship_Zip").value=document.getElementById("Ship_Zip").value;
    document.getElementById("fr_Bill_First_Name").value=document.getElementById("Bill_First_Name").value;
    document.getElementById("fr_Bill_Last_Name").value=document.getElementById("Bill_Last_Name").value;
    document.getElementById("fr_Bill_Address1").value=document.getElementById("Bill_Address1").value;
    document.getElementById("fr_Bill_Address2").value=document.getElementById("Bill_Address2").value;
    document.getElementById("fr_Bill_City").value=document.getElementById("Bill_City").value;
    document.getElementById("fr_Bill_State").value=document.getElementById("Bill_State").value;
    document.getElementById("fr_Bill_Zip").value=document.getElementById("Bill_Zip").value;
    document.getElementById("fr_Bill_Type").value=document.getElementById("Bill_Type").value;
    document.getElementById("fr_Bill_Number").value=document.getElementById("Bill_Number").value;
    document.getElementById("fr_Bill_Holder").value=document.getElementById("Bill_Holder").value;
    document.getElementById("fr_Bill_Expire").value=document.getElementById("Bill_Expire").value;
    var alertMessage ="";
    if (document.getElementById("fr_Ship_First_Name").value.length<1)
    {
        alertMessage+="Shipping First Name cannot be empty!\n\n";
    }
    if (document.getElementById("fr_Ship_Last_Name").value.length<1)
    {
        alertMessage+="Shipping Last Name cannot be empty!\n\n";
    }
    if (document.getElementById("fr_Ship_Address1").value.length<1)
        alertMessage+="Shipping Address cannot be empty!\n\n";
    if (document.getElementById("fr_Ship_City").value.length<1)
        alertMessage+="Shipping City cannot be empty!\n\n";
    if (document.getElementById("fr_Ship_State").value.length<1)
        alertMessage+="Shipping State cannot be empty!\n\n";
    if (document.getElementById("fr_Ship_Zip").value.length!=5 || isNaN(document.getElementById("Ship_Zip").value))
        alertMessage+="Shipping Zip must be 5 digits!\n\n";
    if (document.getElementById("fr_Bill_First_Name").value.length<1)
        alertMessage+="Billing First Name cannot be empty!\n\n";
    if (document.getElementById("fr_Ship_Last_Name").value.length<1)
    {
        alertMessage+="Billing Last Name cannot be empty!\n\n";
    }
    if (document.getElementById("fr_Bill_Address1").value.length<1)
        alertMessage+="Billing Address cannot be empty!\n\n";
    if (document.getElementById("fr_Bill_City").value.length<1)
        alertMessage+="Billing City cannot be empty!\n\n";
    if (document.getElementById("fr_Bill_State").value.length<1)
        alertMessage+="Billing State cannot be empty!\n\n";
    if (document.getElementById("Bill_Holder").value.length<1)
        alertMessage+="Card Holder cannot be empty!\n\n";
    if (document.getElementById("fr_Bill_Zip").value.length!=5 || isNaN(document.getElementById("Bill_Zip").value))
        alertMessage+="Billing Zip must be 5 digits!\n\n";
    if (document.getElementById("fr_Bill_Number").value.length!=16 || isNaN(document.getElementById("Bill_Number").value))
        alertMessage+="Card Number must be 16 digits!\n\n";


    if (alertMessage.length>1)
    {
        alert(alertMessage);

    }else
    {
        document.getElementById("fr_Info").submit();
    }


    //var hotlist = 1;
    //"<?php echo $_SESSION['row_number'] ?>";
    ///if(hotlist != 0){
    ////	alert("Failed!");
    //}else{
    //		document.getElementById("fr_Info").submit();
    //	}

}

function setAddress(first_Name, last_Name, address1, address2, city, state, zip)
{
    document.getElementById("Ship_First_Name").value=first_Name;
    document.getElementById("Ship_Last_Name").value=last_Name;
    document.getElementById("Ship_Address1").value=address1;
    document.getElementById("Ship_Address2").value=address2;
    document.getElementById("Ship_City").value=city;
    document.getElementById("Ship_State").value=state;
    document.getElementById("Ship_Zip").value=zip;
}
function setBill(account, holder, expire, type, first_Name, last_Name, address1, address2, city, state, zip)
{
    document.getElementById("Bill_Number").value=account;
    document.getElementById("Bill_Holder").value=holder;
    document.getElementById("Bill_Expire").value=expire;
    document.getElementById("Bill_Type").value=type;
    document.getElementById("Bill_First_Name").value=first_Name;
    document.getElementById("Bill_Last_Name").value=last_Name;
    document.getElementById("Bill_Address1").value=address1;
    document.getElementById("Bill_Address2").value=address2;
    document.getElementById("Bill_City").value=city;
    document.getElementById("Bill_State").value=state;
    document.getElementById("Bill_Zip").value=zip;


}
function copyTo()
{
    document.getElementById("Bill_First_Name").value=document.getElementById("Ship_First_Name").value;
    document.getElementById("Bill_Last_Name").value=document.getElementById("Ship_Last_Name").value;
    document.getElementById("Bill_Address1").value=document.getElementById("Ship_Address1").value;
    document.getElementById("Bill_Address2").value=document.getElementById("Ship_Address2").value;
    document.getElementById("Bill_City").value=document.getElementById("Ship_City").value;
    document.getElementById("Bill_State").value=document.getElementById("Ship_State").value;
    document.getElementById("Bill_Zip").value=document.getElementById("Ship_Zip").value;

}
function saveAddress()
{
    var alertMessage ="";
    if (document.getElementById("Ship_First_Name").value.length<1)
        alertMessage+="First Name cannot be empty!\n\n";
    if (document.getElementById("Ship_Address1").value.length<1)
        alertMessage+="Address1 cannot be empty!\n\n";
    if (document.getElementById("Ship_City").value.length<1)
        alertMessage+="City cannot be empty!\n\n";
    if (document.getElementById("Ship_State").value.length<1)
        alertMessage+="State cannot be empty!\n\n";
    if (document.getElementById("Ship_Zip").value.length!=5 || isNaN(document.getElementById("Ship_Zip").value))
        alertMessage+="Zip must be 5 digits!\n\n";
    // if (!(/\S+@\S+\.\S+/.test(uname)))
    // 	alertMessage+="Username should be a valid email.\n(i.e. <text>@<text>.<text>)\n\n";
    // if (pword.length<1)
    // 	alertMessage+="Password cannot be blank.\n\n";
    if (alertMessage.length>1)
        alert(alertMessage);
    else
    {
        document.getElementById("Ship_Info").submit();
    }
}
function saveCard()
{
    var alertMessage ="";
    if (document.getElementById("Bill_First_Name").value.length<1)
        alertMessage+="First Name cannot be empty!\n\n";
    if (document.getElementById("Bill_Address1").value.length<1)
        alertMessage+="Address1 cannot be empty!\n\n";
    if (document.getElementById("Bill_City").value.length<1)
        alertMessage+="City cannot be empty!\n\n";
    if (document.getElementById("Bill_State").value.length<1)
        alertMessage+="State cannot be empty!\n\n";
    if (document.getElementById("Bill_Zip").value.length!=5 || isNaN(document.getElementById("Bill_Zip").value))
        alertMessage+="Zip must be 5 digits!\n\n";
    if (document.getElementById("Bill_Number").value.length!=16 || isNaN(document.getElementById("Bill_Number").value))
        alertMessage+="Card Number must be 16 digits!\n\n";
    if (alertMessage.length>1)
        alert(alertMessage);
    else
    {
        document.getElementById("Bill_Info").submit();
    }
}
function resetShippingInfo()
{
    document.getElementById("Ship_First_Name").value="";
    document.getElementById("Ship_Last_Name").value="";
    document.getElementById("Ship_Address1").value="";
    document.getElementById("Ship_Address2").value="";
    document.getElementById("Ship_City").value="";
    document.getElementById("Ship_State").value="";
    document.getElementById("Ship_Zip").value="";
}
function resetBillingInfo()
{
    document.getElementById("Bill_Number").value="";
    document.getElementById("Bill_Holder").value="";
    document.getElementById("Bill_Expire").value="";
    document.getElementById("Bill_Type").value="";
    document.getElementById("Bill_First_Name").value="";
    document.getElementById("Bill_Last_Name").value="";
    document.getElementById("Bill_Address1").value="";
    document.getElementById("Bill_Address2").value="";
    document.getElementById("Bill_City").value="";
    document.getElementById("Bill_State").value="";
    document.getElementById("Bill_Zip").value="";
}
// function checkLogin()
// {
// 	var uname = document.getElementById("uname").value;
// 	var pword = document.getElementById("pword").value;
// 	var alertMessage ="";
// 	if (uname.length<4)
// 		alertMessage+="Username should be longer than three characters.\n\n";
// 	if (!(/\S+@\S+\.\S+/.test(uname)))
// 		alertMessage+="Username should be a valid email.\n(i.e. <text>@<text>.<text>)\n\n";
// 	if (pword.length<1)
// 		alertMessage+="Password cannot be blank.\n\n";
// 	if (alertMessage.length>1)
// 		alert(alertMessage);
// 	else
// 	{
// 		document.getElementById("btnSubmit").style.display = "none";
// 		document.getElementById("formLogin").submit();
// 	}
// }